package com.mycompany.myapp.business.Parser;
import com.mycompany.myapp.business.Parser.pojo.ResultParseFormat;
import java.util.List;

/**
 * Created by cda09d on 22/06/2016.
 */

public class SearchParse {
    // getResult a besoin qu'on lui passe en parametre un parser qui sera en type IParse
    // pour permettre d'utiliser Strategy en appelent la methode parse contenu dans chaque parser
    // definit dans l'interface IPArser
     public List<ResultParseFormat> getResults(IParser parser) {
        return parser.parse();
    }
}
