package com.mycompany.myapp.business.Parser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;


import com.mycompany.myapp.business.Parser.pojo.ResultParseFormat;


/**
 * Created by cda09d on 22/06/2016.
 */
public class ParserStack implements IParser {
    private String srcApi;
    private List<ResultParseFormat> l= new ArrayList<>();
    private String textSearch;
    private String url="http://api.stackexchange.com/2.2/questions?order=desc&site=stackoverflow&search=";



    public ParserStack(String srcApi,String textSearch) {
        this.srcApi = srcApi;
        this.textSearch = textSearch;
        this.url += textSearch ;
    }


    @Override
    public List<ResultParseFormat> parse() {
        // Convert JSON string to Object

        try {
            // Convert JSON string to Object
            URL urlDemo = new URL(url);  // declarer l'url de l'api stackExchange
            URLConnection yc = urlDemo.openConnection();  // se connecter l'url de l'api stackExchange
            yc.setDoOutput(true); // ogligatoire pour permettre d'utiliser le resultat en sortie
            // recupere en et dezip en streaming car stack api retourne un json gzippé
            BufferedReader resultJson = new BufferedReader (new InputStreamReader(   (new GZIPInputStream(yc.getInputStream () )) ));
            String resultatJson = IOUtils.toString(resultJson); // recupere le buffer result json et le met dans une String


            JsonElement jElement = new JsonParser().parse(resultatJson);
            JsonObject jObject = jElement.getAsJsonObject();
            JsonArray items = jObject.getAsJsonArray("items");

            for (JsonElement element : items) {
                JsonObject myItem = element.getAsJsonObject();
                String title = myItem.get("title").getAsString();
                String link = myItem.get("link").getAsString();
                //System.out.println("=>"+title);
                l.add(new ResultParseFormat(this.srcApi,title,link));
               /*JsonObject owner = myItem.getAsJsonObject("owner");  // si on est face a un objet  dans items suivant la structure du json
               owner.get("login")..*/

            }

            return l;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return l;
    }
}

/*  version quentin



   package com.javaproject.com.mycompany.myapp.business.ws;
    ​
    import com.google.gson.JsonArray;
    import com.google.gson.JsonElement;
    import com.google.gson.JsonObject;
    import com.google.gson.JsonParser;
    import org.apache.commons.io.IOUtils;
    import java.io.BufferedReader;
    import java.io.IOException;
    import java.io.InputStreamReader;
    import java.net.URL;
    import java.net.URLConnection;
    import java.util.ArrayList;
    import java.util.List;
    import java.util.zip.GZIPInputStream;
    ​
public class GsonSearchAPIStackOverFlow {
    public static void main(String[] args) {
        GsonSearchAPIStackOverFlow api1 = new GsonSearchAPIStackOverFlow();
        api1.jsonToJava("javascript");
    }
    ​
    public List<ResultAPI> jsonToJava(String search){
        List<ResultAPI> result = new ArrayList<>();
        ResultAPI resultAPI = new ResultAPI();
        try {
            String url = "http://api.stackexchange.com/2.2/questions?order=desc&site=stackoverflow&search="+search;
            // Convert JSON string to Object
            URL urlDemo = new URL(url);
            URLConnection yc = urlDemo.openConnection();
            yc.setDoOutput(true);
            BufferedReader resultJson = new BufferedReader (new InputStreamReader(   (new GZIPInputStream(yc.getInputStream () )) ));
            String resultatJson = IOUtils.toString(resultJson);
            ​
            JsonElement jElement = new JsonParser().parse(resultatJson);
            JsonObject jObject = jElement.getAsJsonObject();
            JsonArray items = jObject.getAsJsonArray("items");
            ​
            for (JsonElement e:items
                ) {
                JsonObject item = e.getAsJsonObject();
                resultAPI.setDescription(item.get("tags").toString());
                resultAPI.setTitle(item.get("title").toString());
                resultAPI.setUrl(item.get("link").toString());
                //System.out.println(resultAPI.getDescription()+resultAPI.getUrl()+resultAPI.getTitle());
                result.add(resultAPI);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
*/
