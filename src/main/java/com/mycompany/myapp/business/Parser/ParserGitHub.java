package com.mycompany.myapp.business.Parser;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;


import com.mycompany.myapp.business.Parser.pojo.ResultParseFormat;


/**
 * Created by cda09d on 22/06/2016.
 */
public class ParserGitHub implements IParser {
     private  String srcApi;
     private List<ResultParseFormat> l = new ArrayList<>();
     private String textSearch;
     private String url="https://api.github.com/search/repositories?sort=stars&order=desc&q=";



    public ParserGitHub(String srcApi,String textSearch) {
           this.srcApi = srcApi;
           this.textSearch = textSearch;
           this.url += textSearch ; //+ "+language:assembly";

    }

    @Override
    public List<ResultParseFormat> parse() {


        try {
            String resultatJson = IOUtils.toString(new URL(url)); // recupere le json et le met dans une String
            // System.out.println(resultatJson);
            // doc http://pastebin.com/MDgSRYHR
            JsonElement jelement = new JsonParser().parse(resultatJson);
            JsonObject jobject = jelement.getAsJsonObject(); // recuperer les donnes a partir  du premier {  ((donnees completes)
            JsonArray items = jobject.getAsJsonArray("items"); // on se positionne sur l'items   collection[] d'items c'est un array
            ParseUtil checkCote= new ParseUtil();
            String tmp;
            for (JsonElement element : items) {
                // System.out.println(element.toString());

                JsonObject myItem = element.getAsJsonObject();
              // tmp=checkCote.replaceCote("description",myItem);

                String title =checkCote.replaceCote("description",myItem); // myItem.get("description").getAsString(); //System.out.println("=>title="+title);

                String link =checkCote.replaceCote("html_url",myItem); // myItem.get("html_url").getAsString();     // System.out.println("=>link="+link);

               // System.out.println("=>title= "+ myItem.get("description").getAsString() +" url="+myItem.get("link").getAsString());
                l.add(new ResultParseFormat(this.srcApi, title,  link ))   ;
               /*JsonObject owner = myItem.getAsJsonObject("owner");
               owner.get("login")..*/
            }

           // l.add(new ResultParseFormat("titregit 2"," url git2"))   ;
           // l.add(new ResultParseFormat("titregit 3"," url git3"))   ;
            return l;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
        //return null;
    }

}
