package com.mycompany.myapp.business.Parser;

import com.mycompany.myapp.business.Parser.pojo.ResultParseFormat;

import java.util.List;

/**
 * Created by cda09d on 22/06/2016.
 */
public interface IParser {

    public List<ResultParseFormat> parse();

}
