package com.mycompany.myapp.business.Parser.pojo;


public class ResultParseFormat {
    private String srcApi;
    private String title;
    private String url;

    public ResultParseFormat(String srcApi, String title, String url) {
        this.srcApi = srcApi;
        this.title = title;
        this.url = url;
    }

    public String getSrcApi() {  return srcApi;  }

    public void setSrcApi(String srcApi) {  this.srcApi = srcApi;  }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
