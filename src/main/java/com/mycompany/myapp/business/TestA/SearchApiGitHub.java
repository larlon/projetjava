package com.mycompany.myapp.business.TestA;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;

/**
 * Created by Johnny on 21/06/2016.
 */
public class SearchApiGitHub {

            //Déclaration de l'URL dans url

    String url = "https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc";

    public void run() {
        try {
            String resultatJson = IOUtils.toString(new URL(url));
            System.out.println(resultatJson);

            // JACKSON
            // Parsage du JSON dans les objets InfoGitHub & ReposGitHub
            ObjectMapper mapperGitHub = new ObjectMapper();


            mapperGitHub.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            RepoGitHub infos = mapperGitHub.readValue(resultatJson, RepoGitHub.class);

            //System.out.println("===> " + infos.getItems());
            for (InfoGitHub list : infos.getItems()){
                System.out.println("name " + list.getName());
                System.out.println("description " + list.getDescription());
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }        // Parsage du JSON dans les objets InfoGitHub & ReposGitHub
        //      ObjectMapper mapperGitHub = new ObjectMapper();            // Convert JSON string from file to Object
           /*RepoGitHub infos = mapperGitHub.readValue(resultatJson, RepoGitHub.class);
           System.out.println(RepoGitHub);*/
    }
}




