package com.mycompany.myapp.business.TestA;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InfoGitHub {
    public String getName() {
    return name;
}

    public void setName(String name) {
    this.name = name;
}

    public InfoGitHub() {
        // vide
    }

    @JsonProperty("name")
    public String name;

    /*@JsonProperty("html_url")
    public String html_url;*/

    @JsonProperty("description")
    public String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
