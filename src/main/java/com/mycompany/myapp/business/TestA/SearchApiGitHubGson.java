package com.mycompany.myapp.business.TestA;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Johnny on 22/06/2016.
 */
public class SearchApiGitHubGson {

    String url = "https://api.github.com/search/repositories?q=chipset&sort=stars&order=desc";//"https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc";

    public void run () {
        try{

            String resultatJson = IOUtils.toString(new URL(url));
            System.out.println(resultatJson);

            JsonElement jelement = new JsonParser().parse(resultatJson);

            JsonObject jobject = jelement.getAsJsonObject();
            JsonArray items = jobject.getAsJsonArray("items");

            for ( JsonElement element : items )
            {
                //System.out.println(element.toString());
                JsonObject myItem = element.getAsJsonObject();


                String name = myItem.get("name").toString();
                System.out.println(name);

                /*JsonObject owner = myItem.getAsJsonObject("owner");
                owner.get("login")..*/

            }

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } {

        }

    }
}
