package com.mycompany.myapp.business.TestA;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;


public class RepoGitHub

{

    @JsonProperty("Items")
private List<InfoGitHub> Items;

    public List<InfoGitHub> getItems() {
        return Items;
    }

    public void setItems(List<InfoGitHub> Items) {
        this.Items = Items;
    }
}
