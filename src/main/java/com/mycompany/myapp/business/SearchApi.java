package com.mycompany.myapp.business;

import com.mycompany.myapp.business.Parser.ParserGitHub;
import com.mycompany.myapp.business.Parser.ParserStack;
import com.mycompany.myapp.business.Parser.SearchParse;
import com.mycompany.myapp.business.Parser.pojo.ResultParseFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by cda09d on 23/06/2016.
 */
public class SearchApi {
    private String textSearch;

    public SearchApi(String textSearch) {
        this.textSearch = textSearch;
    }
    public String getTextSearch() {
        return textSearch;
    }
    public void setTextSearch(String textSearch) {
        this.textSearch = textSearch;
    }

    public List<ResultParseFormat> getSearchParse() {
       // List<ResultParseFormat> r= new ArrayList<>();
       // List<ResultParseFormat> rGit= new ArrayList<>();
        List<Future<List<ResultParseFormat>>> rFutur = null; // creer un future qui recevra le result des execution des lmba Callable
        SearchParse s = new SearchParse(); // mise en place de la strategie Search en vue d'utilisation de l'interface IParser


        Callable<List<ResultParseFormat>> task1= () -> {
            List<ResultParseFormat> l= new ArrayList<>();
            l.addAll(s.getResults( new ParserStack("StackOverFlow",textSearch) ));
            System.out.println("=>List  task1 stack ");
            return l;  // return obligatoure car on utilise l'interface Callable

        };

        Callable<List<ResultParseFormat>> task2= () -> {
            List<ResultParseFormat> l= new ArrayList<>();
            l.addAll(s.getResults( new ParserGitHub("GitHub",textSearch) ));
            System.out.println("=>List  task2 Git ");
            return l;  // return obligatoure car on utilise l'interface Callable

        };

        // creer et utiliser uen executor pour les callables
        // cet executor lancere a en multitthred mles tache dans la liste de callables ci dessous
        ExecutorService executor = Executors.newWorkStealingPool();
        // definir la liste the Htread (de Callable) a executer
        List<Callable<List<ResultParseFormat>>> callables = new ArrayList<>();
        // ajouter les taches a excuter a la list
        callables.add(task2);
        callables.add(task1);



        List<ResultParseFormat> r = new ArrayList<>();
        //utiliser un futur pour recuperer les resultats
        try {

            rFutur = executor.invokeAll(callables);
            for (Future<List<ResultParseFormat>> fr : rFutur)
            {
                r.addAll(fr.get());

            }

            return r;
        }  catch (InterruptedException |ExecutionException e ) {
            e.printStackTrace();

        }



        System.out.println(" =============== resSearchAPI" );
      /*  for (ResultParseFormat a :  r) {
            System.out.println("resSearchAPI =>" + a.getSrcApi()  + " " + a.getTitle()  );
        } */


       // r.addAll(s.getResults( new ParserStack("StackOverFlow",textSearch) ));
       // r.addAll(s.getResults( new ParserGitHub("GitHub",textSearch) ));


       /* for(ResultParseFormat str1:r)
           System.out.println(str1.getSrcApi() + " " + str1.getTitle() +" " + str1.getUrl());*/


       // return  r;
        return null;
    }





    /*
    public List<ResultParseFormat> getSearchParseOK() {
        List<ResultParseFormat> r= new ArrayList<>();

        SearchParse s = new SearchParse(); // mise en place de la strategie Search en vue d'utilisation de l'interface IParser
        r.addAll(s.getResults( new ParserStack("StackOverFlow",textSearch) ));
        r.addAll(s.getResults( new ParserGitHub("GitHub",textSearch) ));


       // for(ResultParseFormat str1:r)
          // System.out.println(str1.getSrcApi() + " " + str1.getTitle() +" " + str1.getUrl());
        return  r;
    }*/





}
