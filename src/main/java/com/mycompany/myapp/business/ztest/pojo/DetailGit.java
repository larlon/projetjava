package com.mycompany.myapp.business.ztest.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by cda09d on 22/06/2016.
 */



public class DetailGit {
    @JsonProperty("description")
    private String description;
    @JsonProperty("url")
    private String url;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
