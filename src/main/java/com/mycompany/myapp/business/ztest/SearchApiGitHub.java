package com.mycompany.myapp.business.ztest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.mycompany.myapp.business.ztest.pojo.DetailGit;
import com.mycompany.myapp.business.ztest.pojo.ResultParse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;


/**
 * Created by cda09d on 21/06/2016.
 */
// http://www.freeformatter.com/json-formatter.html#ad-output


public class SearchApiGitHub {

    //private String url = "https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc";
    private String url = "https://api.github.com/search/repositories?q=blog+language:assembly&sort=stars&order=desc";



    public void run() {
        try {
            // methode jackson a besoin d'utiliser autant de class pojo que de sousniveau dans le json retrouné

            String resultatJson = IOUtils.toString(new URL(url));
            System.out.println(resultatJson);
//JSON from String to Object
            ObjectMapper mapper = new ObjectMapper();
            // permet de configuerer le mappage et ne prendre que les element annoté
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            ResultParse obj = mapper.readValue(resultatJson, ResultParse.class);

            System.out.println("obj = " +obj.getItems());

            for(DetailGit it : obj.getItems()) {
                System.out.println("titre=" + it.getDescription());
                System.out.println("url=" + it.getUrl());
            }


  /*          for(DetailGit  dt : ResultParse)
                System.out.println(dt);
*/
        } catch (MalformedURLException e) {
            e.printStackTrace();
         } catch (IOException e) {
            e.printStackTrace();
        }
    }










}
