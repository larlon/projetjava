package com.mycompany.myapp.business.ztest;

import java.io.IOException;
import java.net.URL;


import com.mycompany.myapp.business.ztest.pojo.ObjectJson;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Created by cda09d on 21/06/2016.
 */

// doc stack exchange https://api.stackexchange.com/
// https://api.stackexchange.com/docs/
// https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&body=javascript%20array&site=stackoverflow


// git hub
//https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc

public class SearchApiStackOverflow {
    public static void main(String[] args) {
        SearchApiStackOverflow obj = new SearchApiStackOverflow();
        obj.run();
    }

   /* private void run() {
        String textSearch="jquery";
        String url="https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&title=jquery&site=stackoverflow";

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = "{'name' : 'mkyong'}";


       //JSON from URL to Object
       // Staff obj = mapper.readValue(new URL("http://mkyong.com/api/staff.json"), Staff.class);
        //https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&title=jquery&site=stackoverflow

        Staff obj = mapper.readValue(new URL("https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&title=jquery&site=stackoverflow"), Staff.class);
    }*/

    public void run() {





        ObjectMapper mapper = new ObjectMapper();
        // permet de configuerer le mappage et ne prendre que les element annoté
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            // Convert JSON string to Object


            ObjectJson jsonInString = mapper.readValue(new URL("http://api.stackexchange.com/2.2/questions?order=desc&search=javascript&site=stackoverflow"), ObjectJson.class);

            System.out.println(jsonInString.getItems());

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




/*


q.mer [9:25 AM]
public class SearchAPIStackOverFlow {
   public static void main(String[] args) {
       SearchAPIStackOverFlow obj = new SearchAPIStackOverFlow();
//        System.out.println("at");
       obj.run();
   }

   private void run() {
       ObjectMapper mapper = new ObjectMapper();
       mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
       try {
           // Convert JSON string to Object
           ObjectJson jsonInString = mapper.readValue(new URL("http://api.stackexchange.com/2.2/questions?order=desc&search=javascript&site=stackoverflow"), ObjectJson.class);

           System.out.println(jsonInString.getItems());

       } catch (JsonGenerationException e) {
           e.printStackTrace();
       } catch (JsonMappingException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
   }

}

// en json  [] =  une collection
// en json  { est un objet

 */
