package com.mycompany.myapp.business.ztest.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by cda09d on 22/06/2016.
 */
public class Repo {

    @JsonProperty("title")
    private String title;
    @JsonProperty("link")
    private String link;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}




