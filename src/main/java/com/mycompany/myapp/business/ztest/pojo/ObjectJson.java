package com.mycompany.myapp.business.ztest.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by cda09d on 22/06/2016.
 */
public class ObjectJson {
    @JsonProperty("items")
   private List<Repo> Items;

    public List<Repo> getItems() {
        return Items;
    }

    public void setItems(List<Repo> Items) {
        this.Items = Items;
    }

    public ObjectJson() {

    }
}
