package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Csearchkey;
import com.mycompany.myapp.repository.CsearchkeyRepository;
import com.mycompany.myapp.repository.search.CsearchkeySearchRepository;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Csearchkey.
 */
@RestController
@RequestMapping("/api")
public class CsearchkeyResource {

    private final Logger log = LoggerFactory.getLogger(CsearchkeyResource.class);
        
    @Inject
    private CsearchkeyRepository csearchkeyRepository;
    
    @Inject
    private CsearchkeySearchRepository csearchkeySearchRepository;
    
    /**
     * POST  /csearchkeys : Create a new csearchkey.
     *
     * @param csearchkey the csearchkey to create
     * @return the ResponseEntity with status 201 (Created) and with body the new csearchkey, or with status 400 (Bad Request) if the csearchkey has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/csearchkeys",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Csearchkey> createCsearchkey(@RequestBody Csearchkey csearchkey) throws URISyntaxException {
        log.debug("REST request to save Csearchkey : {}", csearchkey);
        if (csearchkey.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("csearchkey", "idexists", "A new csearchkey cannot already have an ID")).body(null);
        }
        Csearchkey result = csearchkeyRepository.save(csearchkey);
        csearchkeySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/csearchkeys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("csearchkey", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /csearchkeys : Updates an existing csearchkey.
     *
     * @param csearchkey the csearchkey to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated csearchkey,
     * or with status 400 (Bad Request) if the csearchkey is not valid,
     * or with status 500 (Internal Server Error) if the csearchkey couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/csearchkeys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Csearchkey> updateCsearchkey(@RequestBody Csearchkey csearchkey) throws URISyntaxException {
        log.debug("REST request to update Csearchkey : {}", csearchkey);
        if (csearchkey.getId() == null) {
            return createCsearchkey(csearchkey);
        }
        Csearchkey result = csearchkeyRepository.save(csearchkey);
        csearchkeySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("csearchkey", csearchkey.getId().toString()))
            .body(result);
    }

    /**
     * GET  /csearchkeys : get all the csearchkeys.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of csearchkeys in body
     */
    @RequestMapping(value = "/csearchkeys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Csearchkey> getAllCsearchkeys() {
        log.debug("REST request to get all Csearchkeys");
        List<Csearchkey> csearchkeys = csearchkeyRepository.findAll();
        return csearchkeys;
    }

    /**
     * GET  /csearchkeys/:id : get the "id" csearchkey.
     *
     * @param id the id of the csearchkey to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the csearchkey, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/csearchkeys/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Csearchkey> getCsearchkey(@PathVariable Long id) {
        log.debug("REST request to get Csearchkey : {}", id);
        Csearchkey csearchkey = csearchkeyRepository.findOne(id);
        return Optional.ofNullable(csearchkey)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /csearchkeys/:id : delete the "id" csearchkey.
     *
     * @param id the id of the csearchkey to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/csearchkeys/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCsearchkey(@PathVariable Long id) {
        log.debug("REST request to delete Csearchkey : {}", id);
        csearchkeyRepository.delete(id);
        csearchkeySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("csearchkey", id.toString())).build();
    }

    /**
     * SEARCH  /_search/csearchkeys?query=:query : search for the csearchkey corresponding
     * to the query.
     *
     * @param query the query of the csearchkey search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/csearchkeys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Csearchkey> searchCsearchkeys(@RequestParam String query) {
        log.debug("REST request to search Csearchkeys for query {}", query);
        return StreamSupport
            .stream(csearchkeySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
