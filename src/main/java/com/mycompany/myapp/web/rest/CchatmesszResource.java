package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Cchatmessz;
import com.mycompany.myapp.repository.CchatmesszRepository;
import com.mycompany.myapp.repository.search.CchatmesszSearchRepository;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Cchatmessz.
 */
@RestController
@RequestMapping("/api")
public class CchatmesszResource {

    private final Logger log = LoggerFactory.getLogger(CchatmesszResource.class);
        
    @Inject
    private CchatmesszRepository cchatmesszRepository;
    
    @Inject
    private CchatmesszSearchRepository cchatmesszSearchRepository;
    
    /**
     * POST  /cchatmesszs : Create a new cchatmessz.
     *
     * @param cchatmessz the cchatmessz to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cchatmessz, or with status 400 (Bad Request) if the cchatmessz has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/cchatmesszs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Cchatmessz> createCchatmessz(@RequestBody Cchatmessz cchatmessz) throws URISyntaxException {
        log.debug("REST request to save Cchatmessz : {}", cchatmessz);
        if (cchatmessz.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("cchatmessz", "idexists", "A new cchatmessz cannot already have an ID")).body(null);
        }
        Cchatmessz result = cchatmesszRepository.save(cchatmessz);
        cchatmesszSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/cchatmesszs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("cchatmessz", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cchatmesszs : Updates an existing cchatmessz.
     *
     * @param cchatmessz the cchatmessz to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cchatmessz,
     * or with status 400 (Bad Request) if the cchatmessz is not valid,
     * or with status 500 (Internal Server Error) if the cchatmessz couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/cchatmesszs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Cchatmessz> updateCchatmessz(@RequestBody Cchatmessz cchatmessz) throws URISyntaxException {
        log.debug("REST request to update Cchatmessz : {}", cchatmessz);
        if (cchatmessz.getId() == null) {
            return createCchatmessz(cchatmessz);
        }
        Cchatmessz result = cchatmesszRepository.save(cchatmessz);
        cchatmesszSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("cchatmessz", cchatmessz.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cchatmesszs : get all the cchatmesszs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cchatmesszs in body
     */
    @RequestMapping(value = "/cchatmesszs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Cchatmessz> getAllCchatmesszs() {
        log.debug("REST request to get all Cchatmesszs");
        List<Cchatmessz> cchatmesszs = cchatmesszRepository.findAll();
        return cchatmesszs;
    }

    /**
     * GET  /cchatmesszs/:id : get the "id" cchatmessz.
     *
     * @param id the id of the cchatmessz to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cchatmessz, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/cchatmesszs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Cchatmessz> getCchatmessz(@PathVariable Long id) {
        log.debug("REST request to get Cchatmessz : {}", id);
        Cchatmessz cchatmessz = cchatmesszRepository.findOne(id);
        return Optional.ofNullable(cchatmessz)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cchatmesszs/:id : delete the "id" cchatmessz.
     *
     * @param id the id of the cchatmessz to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/cchatmesszs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCchatmessz(@PathVariable Long id) {
        log.debug("REST request to delete Cchatmessz : {}", id);
        cchatmesszRepository.delete(id);
        cchatmesszSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cchatmessz", id.toString())).build();
    }

    /**
     * SEARCH  /_search/cchatmesszs?query=:query : search for the cchatmessz corresponding
     * to the query.
     *
     * @param query the query of the cchatmessz search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/cchatmesszs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Cchatmessz> searchCchatmesszs(@RequestParam String query) {
        log.debug("REST request to search Cchatmesszs for query {}", query);
        return StreamSupport
            .stream(cchatmesszSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
