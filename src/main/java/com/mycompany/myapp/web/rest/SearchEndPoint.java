package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.business.Parser.pojo.ResultParseFormat;
import com.mycompany.myapp.service.SearchService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cda09d on 24/06/2016.
 */

@RestController            // definit que cette classe est un REST controller qui sera exposé
@RequestMapping("/api")    // definit sur la classe l'url racine de toute les methodes de la classe qui seront exposées
                           // l'ensemble des methodes qui seront definit dans ce controller auro une documentation disponible dans /docs

// pour tester http://localhost:8080/api/search?q=blal   on a un JSON
// http://localhost:8080/#/docs   pour voir  le detail des API et la doc

public class SearchEndPoint {

    List<ResultParseFormat> r= new ArrayList<>();
    /**
     * GET /search : la liste des resultats de recherche a recuperer
     * @param s : la string recherché
     * @return  ResponseEntity<List<ResultParseFormat> >: le format de reponse avec satos HttpStatus.OK
     */
    @RequestMapping(value="/search",               // definit la route que le front va interroger   comme c'est un controlet on ajotera /api    =>   ipsvsr/api/search?q=XXX
                    method= RequestMethod.GET,        // indique si c'est une methode Get or Post
                    produces = MediaType.APPLICATION_JSON_VALUE
    )
    // @RequestPAram car on et dans du GET  comme ?fff=ffff&ddd=gggg
    // @RequestBody pour du POST dans le body  la requette est recue dans le body
    public ResponseEntity<List<ResultParseFormat> >   newSearch(@RequestParam(value="q") String s) {    // ResponseEntity permet de transformer le result en  MediaType.APPLICATION_JSON_VALUE
        // query sera faite en get   du front
        // si on veut envoyer un objet on fera a post du front
        // appeler le service qui lancera la recherche
        // tester si le retour ou la longueure differente de null  video Boris 3  2329
             //    si null
                // return ResponseEntity.badRequest().header(HeardUtil........
      //  log.debug("REST Request to search : {}",s);
        if (s.isEmpty())
        {   return new ResponseEntity<List<ResultParseFormat>>(r, HttpStatus.NO_CONTENT); }
        else
        {   // avoir la classe searchService   qui a un methode fetchResult(s)
            SearchService searchService = new SearchService();
             r = searchService.fetchResult(s);   //on implemente et on appel le service searchService
           // this.r.add( new ResultParseFormat("truc","rrrr","ttttttttttttttt"));
          // this.r.add( new ResultParseFormat("truc1","rrrr1","1ttttttttttttttt"));
            return new ResponseEntity<List<ResultParseFormat>>(r, HttpStatus.OK);
        }

    }
	










}

// pour tester http://localhost:8080/api/search?q=blal   on a un JSON
// http://localhost:8080/#/docs   pour voir  le detail des API


//exemple d'appel de service
/*
 @Inject
 private SearchService searchService;

 @RequestMapping(value="/films/sample-search",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
 //la methode
 public ResponseEntity<List<ResultParseFormat>> sampleSearch(@RRequestParam(value='searchName'vString SearchName ) {
   if (SearchName.isEmpty()) {
      return new ResponseEntity<String>(HttpStatus.NO_CONTENT ....
    }
    return new ResponseEntity<String>("OK",HttpStatus.OK);
 }

 */









