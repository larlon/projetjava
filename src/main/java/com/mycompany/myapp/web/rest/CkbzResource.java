package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Ckbz;
import com.mycompany.myapp.repository.CkbzRepository;
import com.mycompany.myapp.repository.search.CkbzSearchRepository;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Ckbz.
 */
@RestController
@RequestMapping("/api")
public class CkbzResource {

    private final Logger log = LoggerFactory.getLogger(CkbzResource.class);
        
    @Inject
    private CkbzRepository ckbzRepository;
    
    @Inject
    private CkbzSearchRepository ckbzSearchRepository;
    
    /**
     * POST  /ckbzs : Create a new ckbz.
     *
     * @param ckbz the ckbz to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ckbz, or with status 400 (Bad Request) if the ckbz has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ckbzs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Ckbz> createCkbz(@RequestBody Ckbz ckbz) throws URISyntaxException {
        log.debug("REST request to save Ckbz : {}", ckbz);
        if (ckbz.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("ckbz", "idexists", "A new ckbz cannot already have an ID")).body(null);
        }
        Ckbz result = ckbzRepository.save(ckbz);
        ckbzSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/ckbzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("ckbz", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ckbzs : Updates an existing ckbz.
     *
     * @param ckbz the ckbz to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ckbz,
     * or with status 400 (Bad Request) if the ckbz is not valid,
     * or with status 500 (Internal Server Error) if the ckbz couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ckbzs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Ckbz> updateCkbz(@RequestBody Ckbz ckbz) throws URISyntaxException {
        log.debug("REST request to update Ckbz : {}", ckbz);
        if (ckbz.getId() == null) {
            return createCkbz(ckbz);
        }
        Ckbz result = ckbzRepository.save(ckbz);
        ckbzSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("ckbz", ckbz.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ckbzs : get all the ckbzs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ckbzs in body
     */
    @RequestMapping(value = "/ckbzs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Ckbz> getAllCkbzs() {
        log.debug("REST request to get all Ckbzs");
        List<Ckbz> ckbzs = ckbzRepository.findAll();
        return ckbzs;
    }

    /**
     * GET  /ckbzs/:id : get the "id" ckbz.
     *
     * @param id the id of the ckbz to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ckbz, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/ckbzs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Ckbz> getCkbz(@PathVariable Long id) {
        log.debug("REST request to get Ckbz : {}", id);
        Ckbz ckbz = ckbzRepository.findOne(id);
        return Optional.ofNullable(ckbz)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /ckbzs/:id : delete the "id" ckbz.
     *
     * @param id the id of the ckbz to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/ckbzs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCkbz(@PathVariable Long id) {
        log.debug("REST request to delete Ckbz : {}", id);
        ckbzRepository.delete(id);
        ckbzSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("ckbz", id.toString())).build();
    }

    /**
     * SEARCH  /_search/ckbzs?query=:query : search for the ckbz corresponding
     * to the query.
     *
     * @param query the query of the ckbz search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/ckbzs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Ckbz> searchCkbzs(@RequestParam String query) {
        log.debug("REST request to search Ckbzs for query {}", query);
        return StreamSupport
            .stream(ckbzSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
