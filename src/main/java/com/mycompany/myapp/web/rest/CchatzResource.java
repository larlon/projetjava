package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Cchatz;
import com.mycompany.myapp.repository.CchatzRepository;
import com.mycompany.myapp.repository.search.CchatzSearchRepository;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Cchatz.
 */
@RestController
@RequestMapping("/api")
public class CchatzResource {

    private final Logger log = LoggerFactory.getLogger(CchatzResource.class);
        
    @Inject
    private CchatzRepository cchatzRepository;
    
    @Inject
    private CchatzSearchRepository cchatzSearchRepository;
    
    /**
     * POST  /cchatzs : Create a new cchatz.
     *
     * @param cchatz the cchatz to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cchatz, or with status 400 (Bad Request) if the cchatz has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/cchatzs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Cchatz> createCchatz(@RequestBody Cchatz cchatz) throws URISyntaxException {
        log.debug("REST request to save Cchatz : {}", cchatz);
        if (cchatz.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("cchatz", "idexists", "A new cchatz cannot already have an ID")).body(null);
        }
        Cchatz result = cchatzRepository.save(cchatz);
        cchatzSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/cchatzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("cchatz", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cchatzs : Updates an existing cchatz.
     *
     * @param cchatz the cchatz to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cchatz,
     * or with status 400 (Bad Request) if the cchatz is not valid,
     * or with status 500 (Internal Server Error) if the cchatz couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/cchatzs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Cchatz> updateCchatz(@RequestBody Cchatz cchatz) throws URISyntaxException {
        log.debug("REST request to update Cchatz : {}", cchatz);
        if (cchatz.getId() == null) {
            return createCchatz(cchatz);
        }
        Cchatz result = cchatzRepository.save(cchatz);
        cchatzSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("cchatz", cchatz.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cchatzs : get all the cchatzs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cchatzs in body
     */
    @RequestMapping(value = "/cchatzs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Cchatz> getAllCchatzs() {
        log.debug("REST request to get all Cchatzs");
        List<Cchatz> cchatzs = cchatzRepository.findAll();
        return cchatzs;
    }

    /**
     * GET  /cchatzs/:id : get the "id" cchatz.
     *
     * @param id the id of the cchatz to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cchatz, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/cchatzs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Cchatz> getCchatz(@PathVariable Long id) {
        log.debug("REST request to get Cchatz : {}", id);
        Cchatz cchatz = cchatzRepository.findOne(id);
        return Optional.ofNullable(cchatz)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cchatzs/:id : delete the "id" cchatz.
     *
     * @param id the id of the cchatz to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/cchatzs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCchatz(@PathVariable Long id) {
        log.debug("REST request to delete Cchatz : {}", id);
        cchatzRepository.delete(id);
        cchatzSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cchatz", id.toString())).build();
    }

    /**
     * SEARCH  /_search/cchatzs?query=:query : search for the cchatz corresponding
     * to the query.
     *
     * @param query the query of the cchatz search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/cchatzs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Cchatz> searchCchatzs(@RequestParam String query) {
        log.debug("REST request to search Cchatzs for query {}", query);
        return StreamSupport
            .stream(cchatzSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
