package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Csearchkey;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Csearchkey entity.
 */
public interface CsearchkeySearchRepository extends ElasticsearchRepository<Csearchkey, Long> {
}
