package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Ckbz;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Ckbz entity.
 */
public interface CkbzSearchRepository extends ElasticsearchRepository<Ckbz, Long> {
}
