package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Cchatmessz;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Cchatmessz entity.
 */
public interface CchatmesszSearchRepository extends ElasticsearchRepository<Cchatmessz, Long> {
}
