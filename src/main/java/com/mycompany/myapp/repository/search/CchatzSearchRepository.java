package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Cchatz;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Cchatz entity.
 */
public interface CchatzSearchRepository extends ElasticsearchRepository<Cchatz, Long> {
}
