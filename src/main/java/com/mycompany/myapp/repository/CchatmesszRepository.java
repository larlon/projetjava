package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Cchatmessz;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Cchatmessz entity.
 */
@SuppressWarnings("unused")
public interface CchatmesszRepository extends JpaRepository<Cchatmessz,Long> {

}
