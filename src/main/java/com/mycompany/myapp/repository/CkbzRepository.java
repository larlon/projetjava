package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Ckbz;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Ckbz entity.
 */
@SuppressWarnings("unused")
public interface CkbzRepository extends JpaRepository<Ckbz,Long> {

}
