package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Csearchkey;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Csearchkey entity.
 */
@SuppressWarnings("unused")
public interface CsearchkeyRepository extends JpaRepository<Csearchkey,Long> {

}
