package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Cchatz;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Cchatz entity.
 */
@SuppressWarnings("unused")
public interface CchatzRepository extends JpaRepository<Cchatz,Long> {

}
