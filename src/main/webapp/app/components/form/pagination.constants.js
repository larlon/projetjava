(function() {
    'use strict';

    angular
        .module('l4App')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
