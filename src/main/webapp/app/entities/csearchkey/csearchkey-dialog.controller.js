(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CsearchkeyDialogController', CsearchkeyDialogController);

    CsearchkeyDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Csearchkey'];

    function CsearchkeyDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Csearchkey) {
        var vm = this;
        vm.csearchkey = entity;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('l4App:csearchkeyUpdate', result);
			/*console.log('result save key= ' );
			console.log(result);*/
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.csearchkey.id !== null) {
                Csearchkey.update(vm.csearchkey, onSaveSuccess, onSaveError);
            } else {
                Csearchkey.save(vm.csearchkey, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
