(function() {
    'use strict';

    angular
        .module('l4App')
        .factory('CsearchkeySearch', CsearchkeySearch);

    CsearchkeySearch.$inject = ['$resource'];

    function CsearchkeySearch($resource) {
        var resourceUrl =  'api/_search/csearchkeys/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
