(function() {
    'use strict';

    angular
        .module('l4App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('csearchkey', {
            parent: 'entity',
            url: '/csearchkey',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Csearchkeys'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/csearchkey/csearchkeys.html',
                    controller: 'CsearchkeyController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('csearchkey-detail', {
            parent: 'entity',
            url: '/csearchkey/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Csearchkey'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/csearchkey/csearchkey-detail.html',
                    controller: 'CsearchkeyDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Csearchkey', function($stateParams, Csearchkey) {
                    return Csearchkey.get({id : $stateParams.id});
                }]
            }
        })
        .state('csearchkey.new', {
            parent: 'csearchkey',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/csearchkey/csearchkey-dialog.html',
                    controller: 'CsearchkeyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                nb: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('csearchkey', null, { reload: true });
                }, function() {
                    $state.go('csearchkey');
                });
            }]
        })
        .state('csearchkey.edit', {
            parent: 'csearchkey',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/csearchkey/csearchkey-dialog.html',
                    controller: 'CsearchkeyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Csearchkey', function(Csearchkey) {
                            return Csearchkey.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('csearchkey', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('csearchkey.delete', {
            parent: 'csearchkey',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/csearchkey/csearchkey-delete-dialog.html',
                    controller: 'CsearchkeyDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Csearchkey', function(Csearchkey) {
                            return Csearchkey.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('csearchkey', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
