(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CsearchkeyDetailController', CsearchkeyDetailController);

    CsearchkeyDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Csearchkey'];

    function CsearchkeyDetailController($scope, $rootScope, $stateParams, entity, Csearchkey) {
        var vm = this;
        vm.csearchkey = entity;
        
        var unsubscribe = $rootScope.$on('l4App:csearchkeyUpdate', function(event, result) {
            vm.csearchkey = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
