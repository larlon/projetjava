(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CsearchkeyDeleteController',CsearchkeyDeleteController);

    CsearchkeyDeleteController.$inject = ['$uibModalInstance', 'entity', 'Csearchkey'];

    function CsearchkeyDeleteController($uibModalInstance, entity, Csearchkey) {
        var vm = this;
        vm.csearchkey = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Csearchkey.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
