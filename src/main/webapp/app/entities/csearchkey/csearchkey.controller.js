(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CsearchkeyController', CsearchkeyController);

    CsearchkeyController.$inject = ['$scope', '$state', 'Csearchkey', 'CsearchkeySearch'];

    function CsearchkeyController ($scope, $state, Csearchkey, CsearchkeySearch) {
        var vm = this;
        vm.csearchkeys = [];
        vm.loadAll = function() {
            Csearchkey.query(function(result) {
                vm.csearchkeys = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            CsearchkeySearch.query({query: vm.searchQuery}, function(result) {
                vm.csearchkeys = result;
            });
        };
        vm.loadAll();
        
    }
})();
