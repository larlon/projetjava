(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatmesszDialogController', CchatmesszDialogController);

    CchatmesszDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Cchatmessz'];

    function CchatmesszDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Cchatmessz) {
        var vm = this;
        vm.cchatmessz = entity;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('l4App:cchatmesszUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.cchatmessz.id !== null) {
                Cchatmessz.update(vm.cchatmessz, onSaveSuccess, onSaveError);
            } else {
                Cchatmessz.save(vm.cchatmessz, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
