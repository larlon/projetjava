(function() {
    'use strict';
    angular
        .module('l4App')
        .factory('Cchatmessz', Cchatmessz);

    Cchatmessz.$inject = ['$resource'];

    function Cchatmessz ($resource) {
        var resourceUrl =  'api/cchatmesszs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
