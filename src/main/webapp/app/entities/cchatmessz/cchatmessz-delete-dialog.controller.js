(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatmesszDeleteController',CchatmesszDeleteController);

    CchatmesszDeleteController.$inject = ['$uibModalInstance', 'entity', 'Cchatmessz'];

    function CchatmesszDeleteController($uibModalInstance, entity, Cchatmessz) {
        var vm = this;
        vm.cchatmessz = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Cchatmessz.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
