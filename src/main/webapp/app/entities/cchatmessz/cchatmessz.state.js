(function() {
    'use strict';

    angular
        .module('l4App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('cchatmessz', {
            parent: 'entity',
            url: '/cchatmessz',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Cchatmesszs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cchatmessz/cchatmesszs.html',
                    controller: 'CchatmesszController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('cchatmessz-detail', {
            parent: 'entity',
            url: '/cchatmessz/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Cchatmessz'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cchatmessz/cchatmessz-detail.html',
                    controller: 'CchatmesszDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Cchatmessz', function($stateParams, Cchatmessz) {
                    return Cchatmessz.get({id : $stateParams.id});
                }]
            }
        })
        .state('cchatmessz.new', {
            parent: 'cchatmessz',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cchatmessz/cchatmessz-dialog.html',
                    controller: 'CchatmesszDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                idchat: null,
                                author: null,
                                content: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('cchatmessz', null, { reload: true });
                }, function() {
                    $state.go('cchatmessz');
                });
            }]
        })
        .state('cchatmessz.edit', {
            parent: 'cchatmessz',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cchatmessz/cchatmessz-dialog.html',
                    controller: 'CchatmesszDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Cchatmessz', function(Cchatmessz) {
                            return Cchatmessz.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('cchatmessz', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('cchatmessz.delete', {
            parent: 'cchatmessz',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cchatmessz/cchatmessz-delete-dialog.html',
                    controller: 'CchatmesszDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Cchatmessz', function(Cchatmessz) {
                            return Cchatmessz.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('cchatmessz', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
