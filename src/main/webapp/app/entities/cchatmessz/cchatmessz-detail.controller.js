(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatmesszDetailController', CchatmesszDetailController);

    CchatmesszDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Cchatmessz'];

    function CchatmesszDetailController($scope, $rootScope, $stateParams, entity, Cchatmessz) {
        var vm = this;
        vm.cchatmessz = entity;
        
        var unsubscribe = $rootScope.$on('l4App:cchatmesszUpdate', function(event, result) {
            vm.cchatmessz = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
