(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatmesszController', CchatmesszController);

    CchatmesszController.$inject = ['$scope', '$state', 'Cchatmessz', 'CchatmesszSearch'];

    function CchatmesszController ($scope, $state, Cchatmessz, CchatmesszSearch) {
        var vm = this;
        vm.cchatmesszs = [];
        vm.loadAll = function() {
            Cchatmessz.query(function(result) {
                vm.cchatmesszs = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            CchatmesszSearch.query({query: vm.searchQuery}, function(result) {
                vm.cchatmesszs = result;
            });
        };
        vm.loadAll();
        
    }
})();
