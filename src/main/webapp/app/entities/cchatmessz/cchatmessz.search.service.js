(function() {
    'use strict';

    angular
        .module('l4App')
        .factory('CchatmesszSearch', CchatmesszSearch);

    CchatmesszSearch.$inject = ['$resource'];

    function CchatmesszSearch($resource) {
        var resourceUrl =  'api/_search/cchatmesszs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
