(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatzController', CchatzController);

    CchatzController.$inject = ['$scope', '$state', 'Cchatz', 'CchatzSearch'];

    function CchatzController ($scope, $state, Cchatz, CchatzSearch) {
        var vm = this;
        vm.cchatzs = [];
        vm.loadAll = function() {
            Cchatz.query(function(result) {
                vm.cchatzs = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            CchatzSearch.query({query: vm.searchQuery}, function(result) {
                vm.cchatzs = result;
            });
        };
        vm.loadAll();
        
    }
})();
