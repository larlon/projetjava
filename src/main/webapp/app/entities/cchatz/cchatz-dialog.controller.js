(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatzDialogController', CchatzDialogController);

    CchatzDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Cchatz'];

    function CchatzDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Cchatz) {
        var vm = this;
        vm.cchatz = entity;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('l4App:cchatzUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.cchatz.id !== null) {
                Cchatz.update(vm.cchatz, onSaveSuccess, onSaveError);
            } else {
                Cchatz.save(vm.cchatz, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
