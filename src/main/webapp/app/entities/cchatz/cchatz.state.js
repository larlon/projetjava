(function() {
    'use strict';

    angular
        .module('l4App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('cchatz', {
            parent: 'entity',
            url: '/cchatz',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Cchatzs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cchatz/cchatzs.html',
                    controller: 'CchatzController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('cchatz-detail', {
            parent: 'entity',
            url: '/cchatz/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Cchatz'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cchatz/cchatz-detail.html',
                    controller: 'CchatzDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Cchatz', function($stateParams, Cchatz) {
                    return Cchatz.get({id : $stateParams.id});
                }]
            }
        })
        .state('cchatz.new', {
            parent: 'cchatz',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cchatz/cchatz-dialog.html',
                    controller: 'CchatzDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                student: null,
                                operateur: null,
                                state: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('cchatz', null, { reload: true });
                }, function() {
                    $state.go('cchatz');
                });
            }]
        })
        .state('cchatz.edit', {
            parent: 'cchatz',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cchatz/cchatz-dialog.html',
                    controller: 'CchatzDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Cchatz', function(Cchatz) {
                            return Cchatz.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('cchatz', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('cchatz.delete', {
            parent: 'cchatz',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cchatz/cchatz-delete-dialog.html',
                    controller: 'CchatzDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Cchatz', function(Cchatz) {
                            return Cchatz.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('cchatz', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
