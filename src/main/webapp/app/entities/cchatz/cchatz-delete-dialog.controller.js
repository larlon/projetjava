(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatzDeleteController',CchatzDeleteController);

    CchatzDeleteController.$inject = ['$uibModalInstance', 'entity', 'Cchatz'];

    function CchatzDeleteController($uibModalInstance, entity, Cchatz) {
        var vm = this;
        vm.cchatz = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Cchatz.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
