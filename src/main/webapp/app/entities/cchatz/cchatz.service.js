(function() {
    'use strict';
    angular
        .module('l4App')
        .factory('Cchatz', Cchatz);

    Cchatz.$inject = ['$resource'];

    function Cchatz ($resource) {
        var resourceUrl =  'api/cchatzs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
