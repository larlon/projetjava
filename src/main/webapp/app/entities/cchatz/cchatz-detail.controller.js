(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CchatzDetailController', CchatzDetailController);

    CchatzDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Cchatz'];

    function CchatzDetailController($scope, $rootScope, $stateParams, entity, Cchatz) {
        var vm = this;
        vm.cchatz = entity;
        
        var unsubscribe = $rootScope.$on('l4App:cchatzUpdate', function(event, result) {
            vm.cchatz = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
