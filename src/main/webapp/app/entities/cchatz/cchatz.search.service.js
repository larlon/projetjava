(function() {
    'use strict';

    angular
        .module('l4App')
        .factory('CchatzSearch', CchatzSearch);

    CchatzSearch.$inject = ['$resource'];

    function CchatzSearch($resource) {
        var resourceUrl =  'api/_search/cchatzs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
