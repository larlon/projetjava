(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CkbzDialogController', CkbzDialogController);

    CkbzDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Ckbz'];

    function CkbzDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Ckbz) {
        var vm = this;
        vm.ckbz = entity;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('l4App:ckbzUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.ckbz.id !== null) {
                Ckbz.update(vm.ckbz, onSaveSuccess, onSaveError);
            } else {
                Ckbz.save(vm.ckbz, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
