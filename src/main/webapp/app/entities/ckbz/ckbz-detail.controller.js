(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CkbzDetailController', CkbzDetailController);

    CkbzDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Ckbz'];

    function CkbzDetailController($scope, $rootScope, $stateParams, entity, Ckbz) {
        var vm = this;
        vm.ckbz = entity;
        
        var unsubscribe = $rootScope.$on('l4App:ckbzUpdate', function(event, result) {
            vm.ckbz = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
