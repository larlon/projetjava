(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CkbzDeleteController',CkbzDeleteController);

    CkbzDeleteController.$inject = ['$uibModalInstance', 'entity', 'Ckbz'];

    function CkbzDeleteController($uibModalInstance, entity, Ckbz) {
        var vm = this;
        vm.ckbz = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Ckbz.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
