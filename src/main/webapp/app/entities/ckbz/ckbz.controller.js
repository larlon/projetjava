(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('CkbzController', CkbzController);

    CkbzController.$inject = ['$scope', '$state', 'Ckbz', 'CkbzSearch'];

    function CkbzController ($scope, $state, Ckbz, CkbzSearch) {
        var vm = this;
        vm.ckbzs = [];
        vm.loadAll = function() {
            Ckbz.query(function(result) {
                vm.ckbzs = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            CkbzSearch.query({query: vm.searchQuery}, function(result) {
                vm.ckbzs = result;
            });
        };
        vm.loadAll();
        
    }
})();
