(function() {
    'use strict';

    angular
        .module('l4App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('ckbz', {
            parent: 'entity',
            url: '/ckbz',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Ckbzs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ckbz/ckbzs.html',
                    controller: 'CkbzController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('ckbz-detail', {
            parent: 'entity',
            url: '/ckbz/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Ckbz'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ckbz/ckbz-detail.html',
                    controller: 'CkbzDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Ckbz', function($stateParams, Ckbz) {
                    return Ckbz.get({id : $stateParams.id});
                }]
            }
        })
        .state('ckbz.new', {
            parent: 'ckbz',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ckbz/ckbz-dialog.html',
                    controller: 'CkbzDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                question: null,
                                answer: null,
                                author: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('ckbz', null, { reload: true });
                }, function() {
                    $state.go('ckbz');
                });
            }]
        })
        .state('ckbz.edit', {
            parent: 'ckbz',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ckbz/ckbz-dialog.html',
                    controller: 'CkbzDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Ckbz', function(Ckbz) {
                            return Ckbz.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ckbz', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('ckbz.delete', {
            parent: 'ckbz',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ckbz/ckbz-delete-dialog.html',
                    controller: 'CkbzDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Ckbz', function(Ckbz) {
                            return Ckbz.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ckbz', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
