(function() {
    'use strict';
    angular
        .module('l4App')
        .factory('Ckbz', Ckbz);

    Ckbz.$inject = ['$resource'];

    function Ckbz ($resource) {
        var resourceUrl =  'api/ckbzs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
