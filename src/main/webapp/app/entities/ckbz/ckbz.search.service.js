(function() {
    'use strict';

    angular
        .module('l4App')
        .factory('CkbzSearch', CkbzSearch);

    CkbzSearch.$inject = ['$resource'];

    function CkbzSearch($resource) {
        var resourceUrl =  'api/_search/ckbzs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
