(function() {
    'use strict';

    angular
        .module('l4App')
        .directive('clSearch', clSearch);

    clSearch.$inject = [];
    function clSearch(){

        var directive = {
            bindToController: true,
            controller: "SearchController",
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            scope: {
            },
            templateUrl: 'app/page/search/search.html',
        };
        return directive;

        function link(scope, element, attrs) {   }

    } // fin  function clSearch(){

})();
