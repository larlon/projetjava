(function () {   // video boris n�5-> 784
     'use strict';

    angular
        .module('l4App')
        .factory('SearchService',SearchService);

    SearchService.$inject = ['$http'];
	function SearchService ($http) {
	//console.log	('dans service search');
	return function(searchParam) {
		//console.log	('dans service search chaine = ' + searchParam);
		return $http({
			url:"api/search",
			method : "GET",
			params: {q:searchParam}
		}).then(function (response){
			return response.data
		});
	  }
	}


})();