(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('SearchController', SearchController);

	SearchController.$inject = ['$scope', 'Principal','LoginService','SearchService','CsearchkeySearch','Csearchkey'];
	function SearchController ($scope, Principal,LoginService,SearchService,CsearchkeySearch,Csearchkey) {
        var vm = this;
        vm.chaineSearch = "";
		vm.resultSearchKeyWord = [];   // resultat retrouner par l'api de l''entité  csearchkeyword
		vm.resultSearch = [];         // resultat retourné par l'api git et stack
        vm.src = "http"; //srcApi utiliser pour le filtre sur la recherche

        vm.nbResult = {GitHub:0,StackOverFlow:0 }	;
		
		
	    // lancement de la recherche sur les keyword  ou //  doSearchKeyWord(vm.chaineSearch);
        // interroge le REST fournit par L'api service get de  CsearchkeySearch
		vm.getSearchkeyword = function() {
			if (vm.chaineSearch) {
                CsearchkeySearch.query({query: vm.chaineSearch}, function(result) { vm.resultSearchKeyWord = result; /*console.log(vm.resultSearchKeyWord);*/ });
		  }
		}

        //-------------------------------
		// save in base the new keyword en base dans l'entitié csearchkey si c'est un nouveau keyword
        // si on ne le trouve pas deja dans  vm.resultSearchKeyWord
		function putNewKeyword()
		{   var nb=0;
            for (var i=0;i<vm.resultSearchKeyWord.length;i=i+1)
            {   if (vm.resultSearchKeyWord[i].name == vm.chaineSearch)
                { nb=1; break; }
            }
            if (nb==0)
			{  var json = '{"name":"'+vm.chaineSearch+'","nb":"1"}',obj = JSON.parse(json);
		         Csearchkey.save(obj);
                 console.log("save keyword");
			}
		}

    //-------------------------------
	// lancement de la recherche sur les api
		vm.getSearchByString = function() {
            // console.log('getSearchByString= '+ vm.nbFindKeyWord);
		  if (vm.chaineSearch)
		     {  putNewKeyword();
			    doSearch(vm.chaineSearch); //console.log("recherche " + vm.chaineSearch);
		     }
		}

    //-------------------------------
	// appel de l'api par SearchService
		function doSearch(searchParam)
		{
			SearchService(searchParam).then(function(data){
				vm.resultSearch =data;
				if (vm.resultSearch.lenght>0);
				{ vm.nbResult.GitHub=0;vm.nbResult.StackOverFlow=0; /* //console.log("dosearch res",data); //console.log("=>0" +vm.resultSearch[0].srcApi); */
					for (var i=0;i<vm.resultSearch.length;i++)  /*for (var i=0;i<vm.resultSearch.length;i=i+1) console.log(i + "=>" +vm.resultSearch[i].srcApi +vm.resultSearch[i].url); */
					{ //console.log(i + "=>" +vm.resultSearch[i].srcApi +vm.resultSearch[i].url);
						  if (vm.resultSearch[i].srcApi=='GitHub')        vm.nbResult.GitHub++;
						  if (vm.resultSearch[i].srcApi=='StackOverFlow') vm.nbResult.StackOverFlow++;
					}
				}
			/* //console.log("dosearch res",data); //console.log("=>0" +vm.resultSearch[0].srcApi);
			for (var i=0;i<vm.resultSearch.length;i=i+1) console.log(i + "=>" +vm.resultSearch[i].srcApi +vm.resultSearch[i].url); */
			},function() {
				console.log("pas de resultat pour cette recherche");
				$scope.error=' pas de resultat pour cette recherche';
			});
		}

    //-------------------------------
    //filtrer le resulttas de la recherche
        vm.ShowSrcFilter = function(textSrc) {   vm.src = textSrc;  }
	  //-------------------------------
    } // fin function SearchController
})();
