(function() {
    'use strict';

    angular
        .module('l4App')
        .directive('clChat', clChat);

    clChat.$inject = [];
    function clChat(){
        var directive = {
            bindToController: true,
            controller: "ChatController",
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            scope: {
            },
            templateUrl: 'app/page/chat/chat.html',
        };
        return directive;

        function link(scope, element, attrs) { 

		}

    } // fin  function clSearch(){

})();
