	
(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('ChatController',ChatController);

    ChatController.$inject = ['$scope', 'Principal','LoginService','$interval','CchatzSearch','Cchatz','CchatmesszSearch','Cchatmessz','Ckbz','User'];
    function ChatController ($scope, Principal,LoginService,$interval,CchatzSearch,Cchatz,CchatmesszSearch,Cchatmessz,Ckbz,User) {
        var vm = this;
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.profileOperator={ acces: false};
        //vm.register = register;
		
        vm.saisieChat=null;
        vm.checkboxisOperator=true;
		vm.chatEnCours={'idchat':false};  // ,'strict':true
		vm.strict={'strict':true};
		vm.listChat=[];
		vm.NameTarget=""; 
		vm.queryMess="";
		vm.discuss=[];
		vm.fakeAuthor="";
		vm.affDelChat=false;
		
		// variable de la knowledge base
		vm.affAddKB=false;
		vm.ResultKB=[];
		vm.newKB={"question":"question: ","answer":"solution: ","author":"NA"}; // vm.account.login
		
		//variable de adress book
		vm.AddressBook={"state":false,"result":[] };
		
		
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });+

            getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
				//console.log(account);
                vm.isAuthenticated = Principal.isAuthenticated;
                if (vm.isAuthenticated)
                {     loadAllUser();
                    if (vm.account.authorities.includes('ROLE_ADMIN') )
                        vm.profileOperator.acces=true;
					vm.getListChat();
                } // console.log(vm.account);  console.log(vm.account.authorities);  console.log('nbRole = '+  vm.account.authorities.length);
            });
        }

		
        function loadAllUser () {
            User.query(function (result, headers) {
                //hide anonymous user from user management: it's a required user for Spring Security
                for(var i in result) {
                    if(    result[i]['login'] === 'anonymoususer' 
					    || result[i]['login'] === 'system' 
						|| result[i]['login'] === 'admin'
						|| result[i]['login'] === 'user') 
						{
                        result.splice(i,1);
                       }
                }
                vm.AddressBook.result = result;
            });
        }		
		
		
	// lancement de la recherche sur les chatz ;   en fonction de operateur ou student
        // interroge le REST fournit par L'api service get de  CchatzSearch
		// liste des chat en cours pour le user login  
		// constuitre la chaine de requete et interroger le REST
		// format de la chaie de requete  
		//                       query=operateur:operator1  vm.account.login  pour les operateur  ou 
		//                       query=student:student1  vm.account.loginpour les student
		//ex:	http://localhost:9000/api/_search/cchatzs?query=operateur:operator1
		
		vm.getListChat = function() {
			//console.log("vm.getListChat");
			var querylogin="";
			vm.listChat=[];
				if (vm.profileOperator.acces)  querylogin="operateur:"; else querylogin="student:";   //console.log("=>vm.getListChat");
				querylogin+=vm.account.login;  //console.log('querylogin=>' + querylogin)
				CchatzSearch.query(  {query: querylogin}, 
				                         function(result) { 
							             vm.listChat = result;  //console.log(vm.listChat); 
										 if (vm.listChat.length>0)
										 {vm.queryMess="";
											 for (var i=0;i<vm.listChat.length;i++)
											 { if (!vm.chatEnCours.idchat )
												 {
													vm.chatEnCours.idchat=vm.listChat[i].id; 
													var indexChat=islistChat(vm.chatEnCours.idchat);
											        vm.NameTarget="";
											   if (vm.account.login ==vm.listChat[indexChat].student ) 
												    vm.NameTarget=vm.listChat[indexChat].operateur;
											   else vm.NameTarget=vm.listChat[indexChat].student; 
													 
												 }												 
										       vm.queryMess+='idchat:'+vm.listChat[i].id;
											   if (i< vm.listChat.length-1) vm.queryMess+=' || ';
										      } 
											  vm.getListChatMess(vm.queryMess) 
											// for (var i=0;i<vm.resultSearch.length;i=i+1) console.log(i + "=>" +vm.resultSearch[i].srcApi +vm.resultSearch[i].url);
										 //if (vm.chatEnCours.idchat) vm.scrollDown();
										 }
										 else vm.queryMess="";
										 //console.log('vm.queryMess=' + vm.queryMess);
										 });				
		}	

		//ex:	http://localhost:9000/api/_search/CchatmesszSearch?query=idchat:1 || idchat:2
		// http://localhost:9000/api/_search/cchatmesszs?query=idchat%3A1%20%7C%7C%20idchat%3A2%20%7C%7C%20idchat%3A3
		vm.getListChatMess = function(queryMess) {
			vm.discuss =[]
			if (vm.isAuthenticated && vm.listChat.length>0) 
			{    CchatmesszSearch.query(  {query: vm.queryMess},    //'idchat:1 || idchat:3'  // console.log("=>vm.getListChatMess"); console.log('vm.queryMess=>' + vm.queryMess)
				                         function(result) { 
							             vm.discuss = result;  /* console.log(vm.discuss); */ });	
			}	
          	else vm.discuss=[];		
		}

		
		// vm.toggleDelChat()">  $parent.vm.affDelChat
		
        //-------------------------------
		// supprime la conversation idchat en base
		// http://localhost:9000/api/cchatzs/12    id 
		// supprime la conversation idchat dans array listChat[]   aisni que les message associés a cette conversation
        vm.delChat = function() {
			//console.log("delChat => " + vm.chatEnCours.idchat);
			if (vm.chatEnCours.idchat)
			{
			var indexchatSuppr=islistChat(vm.chatEnCours.idchat);
			//var idchatSuppr=islistChat(25);
			if (indexchatSuppr>=0 ) 
			 { //console.log("indexchatSuppr =" + indexchatSuppr);
	            // suppression des messagede la conversation  chat   vm.chatEnCours.idchat
				  for (var i=0;i<vm.discuss.length;i++)
				      {   if (vm.chatEnCours.idchat==vm.discuss[i].idchat)  
						      vm.delChatMessage( vm.discuss[i].id )	; 
						} 
			  // suppression  conversation  chat	 vm.chatEnCours.idchat
		      Cchatz.delete({id: vm.chatEnCours.idchat},
                function () { // remove du array vm.listChat[]
                             vm.listChat.splice(indexchatSuppr, 1); 
                            });
                
				vm.chatEnCours.idchat=null;
				vm.getListChat();
				vm.toggleDelChat();
			 }
			 else console.log("pas dindex trouvé dans listchat"); 				
			}
        }
        vm.toggleDelChat = function() { vm.affDelChat=!vm.affDelChat; console.log("toggleDelChat = " + vm.affDelChat);}
		// supprime le message en base en en front par l'id du message
		// http://localhost:9000/api/cchatmesszs/23  id
        vm.delChatMessage = function(idMessage) { //console.log("delChatMessage => " + idMessage);
			if (idMessage)
			{ Cchatmessz.delete({id: idMessage},
                function () { // remove du array vm.listChat[]
				             var indexChatMessageSuppr=islistDiscuss(idMessage);
							  if (indexChatMessageSuppr)
                                  vm.discuss.splice(indexChatMessageSuppr, 1); 
                            });	
			}
		}

        //-------------------------------
        // return l'index idChat present du tableau listChat sinon renvoi false
        function islistChat(idChat) { 
 			for (var i=0;i<vm.listChat.length;i++)
				{  if (idChat==vm.listChat[i].id) return  i; }
            return false;			
        }
		
        //-------------------------------
        // return l'index idMessage present du tableau vm.discuss sinon renvoi false
        function islistDiscuss(idMessage) {
 			for (var i=0;i<vm.discuss.length;i++)
				{  if (idMessage==vm.discuss[i].id) return  i; }
            return false;			
        }	
		
        //-------------------------------
        // ajoute un message
        vm.addContent = function(MessageContent) {
		//	vm.saisieChat
			
			if (!vm.chatEnCours.idchat) vm.addChat();
			if (vm.chatEnCours.idchat)
			{ 
		      //console.log("addContent = " + MessageContent +" ##");
		  
		    if (MessageContent !== "" && MessageContent !== undefined)
				{ console.log('chat en cours addContent' + vm.chatEnCours.idchat);
				    var owner=vm.account.login;
					if (vm.fakeAuthor) owner=vm.fakeAuthor;
					// save en base REST
					var json = '{"idchat":"'+vm.chatEnCours.idchat+'","author":"'+owner+'","content":"'+MessageContent+'"}',obj = JSON.parse(json);
						 Cchatmessz.save(obj,function(result) { //console.log(result);
								  // vm.discuss.push(result);     //vm.discuss.push({ idchat:vm.chatEnCours.idchat, id:vm.discuss.length,author:owner,content: vm.saisieChat});
					});		
					vm.saisieChat="";vm.fakeAuthor=""; vm.scrollDown("chat1");
					vm.refresh();
				} else console.log("le message est vide");  //console.log(vm.discuss);				
            } 
		}
	    //-------------------------------
        // ajouter un chat
		////{"id":1,"student":"student1","operateur":"operator1","state":1}
        vm.addChat = function() {
			//si le user qui creer le chat n'a pas de chat en cours
			//console.log("addChat");
			//console.log(vm.listChat);
			var ownerChat=vm.account.login;
			var targetChat=vm.affectChat();  
					// save en base REST
					var json='{"student":"' + ownerChat + '","operateur":"' + targetChat + '","state":"1"}',obj = JSON.parse(json);
						 Cchatz.save(obj,function(result) {   console.log(result);
								  //vm.listChat.push(result);  //vm.discuss.push({ idchat:vm.chatEnCours.idchat, id:vm.discuss.length,author:owner,content: vm.saisieChat});
								   vm.listChat[0] = result;
								   console.log(vm.listChat);
								   vm.chatEnCours.idchat=vm.listChat[0].id;
								   //console.log("vm.chatEnCours.idchat => " + vm.listChat[0].id)
					});	
			//console.log("addChat=> " + vm.chatEnCours.idchat);
			vm.refresh();
        }	
		
        //-------------------------------
        // cherche un operateur par random pour lui affecter un nouveau chat en cours de creation 
        vm.affectChat = function() {
			// recuperer la liste des operators
			var listOperator=[{name:"operator1"},{name:"operator2"}];
			var i =  Math.floor(Math.random() * (listOperator.length - 0)) + 0;
			return 	listOperator[i].name;
        }
		
		// click dans le message pour reremplir le formulaire add kb
		vm.pasteQuestionKB = function(text) { vm.newKB.question=text; console.log("pasteQuestionKB=>" + vm.newKB.question); }
		vm.pasteAnswerKB = function(text) { vm.newKB.answer=text; console.log("pasteAnswerKB=>" + vm.newKB.answer); }
		
		
        //-------------------------------
        // recharge les message du chat
		var timer=0;
        $interval(function(){
            timer++;
          //  var idCanal = vm.idC;
            if(timer===100){
				vm.getListChat();
				vm.getAllKB();
				if (vm.chatEnCours.idchat) vm.scrollDown("chat1");
             // console.log("=> timer");
                timer=0;
            }
        },101);
  
      /*  function islistDiscuss(idMessage) {
 			for (var i=0;i<vm.discuss.length;i++)
				{  if (idMessage==vm.discuss[i].id) return  i; }
            return false;			
        }	*/
    //-------------------------------
    //actualise le chat et la knowledge base
     vm.refresh = function(){
				vm.getListChat();
				vm.getAllKB();
		}
  
    //-------------------------------
    //actualise le chat
     vm.scrollDown = function(myDivChat){
		        
				var element = document.getElementById(myDivChat);
				
				console.log("scroll down = " + element.scrollHeight);
                element.scrollTop = (element.scrollHeight);
		}

		
		
		
		
		
    //-------------------------------
    //changer de chat en filtrant sur l'idchat
        vm.changeChat = function(textChat) {  vm.chatEnCours.idchat=textChat;
		                                      var indexChat=islistChat(vm.chatEnCours.idchat);
											  vm.NameTarget="";
											   if (vm.account.login ==vm.listChat[indexChat].student ) vm.NameTarget=vm.listChat[indexChat].operateur;
											   else vm.NameTarget=vm.listChat[indexChat].student;
		                                      vm.scrollDown("chat1");  
											  vm.saisieChat=""; //vm.affAddKB=false ;
											  }	
        vm.toggleOperatorAccess = function(textChat) { if (vm.profileOperator.acces) vm.profileOperator.acces =false ; else vm.profileOperator.acces=true; }	

    // knowledge base
	    // reucpere la liste complete de la knowledge base
	    vm.getAllKB = function() {vm.ResultKB=[]; Ckbz.query(function(result) { vm.ResultKB = result; }); };
		//affiche ou masque la kbase
		vm.toggleAffAddKB = function(textChat) { 
		                       if (vm.affAddKB) { vm.affAddKB=false ; /* vm.newKB.question="question";vm.newKB.answer="solution"; */}
							   else {vm.affAddKB=true;/* vm.newKB.question="question";vm.newKB.answer="solution"; */} 
							   } 
    //adress book
	vm.toggleAdressBook= function() {vm.AddressBook.state=!vm.AddressBook.state;
                                     if (vm.AddressBook.state)	vm.saisieChat=""; 
                                      var aujourdhui = new Date();	 
                                     console.log("toggleAdressBook"+ aujourdhui )

									 }
							   
		
        //-------------------------------
    } // fin function SearchController
})();

