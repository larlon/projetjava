(function() {
    'use strict';

    angular
        .module('l4App')
        .controller('PageController', PageController);

    PageController.$inject = ['$scope', 'Principal', 'LoginService', '$state'];

    function PageController ($scope, Principal, LoginService, $state) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.profileOperator={ acces: false};
        vm.register = register;
        vm.clickChat=false;
        vm.searchTools='searchEngine';
		
		vm.cadrage={"left":"col-md-7","right":"col-md-5"};
		
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });+

        getAccount();
        
        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                if (vm.isAuthenticated)
                {
                    if (vm.account.authorities.includes('ROLE_ADMIN') )
					    {vm.profileOperator.acces=true;
					     vm.clickChat=true;
						 vm.cadrage.left="col-md-4";
						 vm.cadrage.right="col-md-8";
						 } 	 
                }
                // console.log(vm.account);  console.log(vm.account.authorities);  console.log('nbRole = '+  vm.account.authorities.length);
            });
        }
        function register () { $state.go('register');  }

        //-------------------------------
        // affiche ou masque le chat et met le focus dessus
        vm.toggleChat = function() {  if (vm.clickChat) vm.clickChat=null;  else vm.clickChat=true;  
		console.log("vm.toggleChat = "+vm.clickChat);}

    }  //fin function PageController
})();


