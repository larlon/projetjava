(function() {
    'use strict';

    angular
        .module('l4App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('page', {
            parent: 'app',
            url: '/',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/page/page.html',
                    controller: 'PageController',
                    controllerAs: 'vm'
                }
            }
        })
        ;
    }
})();