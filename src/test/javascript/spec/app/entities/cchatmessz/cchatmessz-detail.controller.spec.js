'use strict';

describe('Controller Tests', function() {

    describe('Cchatmessz Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockCchatmessz;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockCchatmessz = jasmine.createSpy('MockCchatmessz');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Cchatmessz': MockCchatmessz
            };
            createController = function() {
                $injector.get('$controller')("CchatmesszDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'l4App:cchatmesszUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
