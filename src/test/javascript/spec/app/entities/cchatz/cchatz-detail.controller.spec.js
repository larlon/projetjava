'use strict';

describe('Controller Tests', function() {

    describe('Cchatz Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockCchatz;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockCchatz = jasmine.createSpy('MockCchatz');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Cchatz': MockCchatz
            };
            createController = function() {
                $injector.get('$controller')("CchatzDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'l4App:cchatzUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
