'use strict';

describe('Controller Tests', function() {

    describe('Ckbz Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockCkbz;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockCkbz = jasmine.createSpy('MockCkbz');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Ckbz': MockCkbz
            };
            createController = function() {
                $injector.get('$controller')("CkbzDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'l4App:ckbzUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
