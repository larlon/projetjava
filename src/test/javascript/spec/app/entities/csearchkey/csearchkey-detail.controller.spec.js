'use strict';

describe('Controller Tests', function() {

    describe('Csearchkey Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockCsearchkey;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockCsearchkey = jasmine.createSpy('MockCsearchkey');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Csearchkey': MockCsearchkey
            };
            createController = function() {
                $injector.get('$controller')("CsearchkeyDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'l4App:csearchkeyUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
