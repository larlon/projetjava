package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.L4App;
import com.mycompany.myapp.domain.Cchatz;
import com.mycompany.myapp.repository.CchatzRepository;
import com.mycompany.myapp.repository.search.CchatzSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CchatzResource REST controller.
 *
 * @see CchatzResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = L4App.class)
@WebAppConfiguration
@IntegrationTest
public class CchatzResourceIntTest {

    private static final String DEFAULT_STUDENT = "AAAAA";
    private static final String UPDATED_STUDENT = "BBBBB";
    private static final String DEFAULT_OPERATEUR = "AAAAA";
    private static final String UPDATED_OPERATEUR = "BBBBB";

    private static final Integer DEFAULT_STATE = 1;
    private static final Integer UPDATED_STATE = 2;

    @Inject
    private CchatzRepository cchatzRepository;

    @Inject
    private CchatzSearchRepository cchatzSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCchatzMockMvc;

    private Cchatz cchatz;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CchatzResource cchatzResource = new CchatzResource();
        ReflectionTestUtils.setField(cchatzResource, "cchatzSearchRepository", cchatzSearchRepository);
        ReflectionTestUtils.setField(cchatzResource, "cchatzRepository", cchatzRepository);
        this.restCchatzMockMvc = MockMvcBuilders.standaloneSetup(cchatzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        cchatzSearchRepository.deleteAll();
        cchatz = new Cchatz();
        cchatz.setStudent(DEFAULT_STUDENT);
        cchatz.setOperateur(DEFAULT_OPERATEUR);
        cchatz.setState(DEFAULT_STATE);
    }

    @Test
    @Transactional
    public void createCchatz() throws Exception {
        int databaseSizeBeforeCreate = cchatzRepository.findAll().size();

        // Create the Cchatz

        restCchatzMockMvc.perform(post("/api/cchatzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cchatz)))
                .andExpect(status().isCreated());

        // Validate the Cchatz in the database
        List<Cchatz> cchatzs = cchatzRepository.findAll();
        assertThat(cchatzs).hasSize(databaseSizeBeforeCreate + 1);
        Cchatz testCchatz = cchatzs.get(cchatzs.size() - 1);
        assertThat(testCchatz.getStudent()).isEqualTo(DEFAULT_STUDENT);
        assertThat(testCchatz.getOperateur()).isEqualTo(DEFAULT_OPERATEUR);
        assertThat(testCchatz.getState()).isEqualTo(DEFAULT_STATE);

        // Validate the Cchatz in ElasticSearch
        Cchatz cchatzEs = cchatzSearchRepository.findOne(testCchatz.getId());
        assertThat(cchatzEs).isEqualToComparingFieldByField(testCchatz);
    }

    @Test
    @Transactional
    public void getAllCchatzs() throws Exception {
        // Initialize the database
        cchatzRepository.saveAndFlush(cchatz);

        // Get all the cchatzs
        restCchatzMockMvc.perform(get("/api/cchatzs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cchatz.getId().intValue())))
                .andExpect(jsonPath("$.[*].student").value(hasItem(DEFAULT_STUDENT.toString())))
                .andExpect(jsonPath("$.[*].operateur").value(hasItem(DEFAULT_OPERATEUR.toString())))
                .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)));
    }

    @Test
    @Transactional
    public void getCchatz() throws Exception {
        // Initialize the database
        cchatzRepository.saveAndFlush(cchatz);

        // Get the cchatz
        restCchatzMockMvc.perform(get("/api/cchatzs/{id}", cchatz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cchatz.getId().intValue()))
            .andExpect(jsonPath("$.student").value(DEFAULT_STUDENT.toString()))
            .andExpect(jsonPath("$.operateur").value(DEFAULT_OPERATEUR.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE));
    }

    @Test
    @Transactional
    public void getNonExistingCchatz() throws Exception {
        // Get the cchatz
        restCchatzMockMvc.perform(get("/api/cchatzs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCchatz() throws Exception {
        // Initialize the database
        cchatzRepository.saveAndFlush(cchatz);
        cchatzSearchRepository.save(cchatz);
        int databaseSizeBeforeUpdate = cchatzRepository.findAll().size();

        // Update the cchatz
        Cchatz updatedCchatz = new Cchatz();
        updatedCchatz.setId(cchatz.getId());
        updatedCchatz.setStudent(UPDATED_STUDENT);
        updatedCchatz.setOperateur(UPDATED_OPERATEUR);
        updatedCchatz.setState(UPDATED_STATE);

        restCchatzMockMvc.perform(put("/api/cchatzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCchatz)))
                .andExpect(status().isOk());

        // Validate the Cchatz in the database
        List<Cchatz> cchatzs = cchatzRepository.findAll();
        assertThat(cchatzs).hasSize(databaseSizeBeforeUpdate);
        Cchatz testCchatz = cchatzs.get(cchatzs.size() - 1);
        assertThat(testCchatz.getStudent()).isEqualTo(UPDATED_STUDENT);
        assertThat(testCchatz.getOperateur()).isEqualTo(UPDATED_OPERATEUR);
        assertThat(testCchatz.getState()).isEqualTo(UPDATED_STATE);

        // Validate the Cchatz in ElasticSearch
        Cchatz cchatzEs = cchatzSearchRepository.findOne(testCchatz.getId());
        assertThat(cchatzEs).isEqualToComparingFieldByField(testCchatz);
    }

    @Test
    @Transactional
    public void deleteCchatz() throws Exception {
        // Initialize the database
        cchatzRepository.saveAndFlush(cchatz);
        cchatzSearchRepository.save(cchatz);
        int databaseSizeBeforeDelete = cchatzRepository.findAll().size();

        // Get the cchatz
        restCchatzMockMvc.perform(delete("/api/cchatzs/{id}", cchatz.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean cchatzExistsInEs = cchatzSearchRepository.exists(cchatz.getId());
        assertThat(cchatzExistsInEs).isFalse();

        // Validate the database is empty
        List<Cchatz> cchatzs = cchatzRepository.findAll();
        assertThat(cchatzs).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCchatz() throws Exception {
        // Initialize the database
        cchatzRepository.saveAndFlush(cchatz);
        cchatzSearchRepository.save(cchatz);

        // Search the cchatz
        restCchatzMockMvc.perform(get("/api/_search/cchatzs?query=id:" + cchatz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cchatz.getId().intValue())))
            .andExpect(jsonPath("$.[*].student").value(hasItem(DEFAULT_STUDENT.toString())))
            .andExpect(jsonPath("$.[*].operateur").value(hasItem(DEFAULT_OPERATEUR.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)));
    }
}
