package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.L4App;
import com.mycompany.myapp.domain.Ckbz;
import com.mycompany.myapp.repository.CkbzRepository;
import com.mycompany.myapp.repository.search.CkbzSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CkbzResource REST controller.
 *
 * @see CkbzResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = L4App.class)
@WebAppConfiguration
@IntegrationTest
public class CkbzResourceIntTest {

    private static final String DEFAULT_QUESTION = "AAAAA";
    private static final String UPDATED_QUESTION = "BBBBB";
    private static final String DEFAULT_ANSWER = "AAAAA";
    private static final String UPDATED_ANSWER = "BBBBB";
    private static final String DEFAULT_AUTHOR = "AAAAA";
    private static final String UPDATED_AUTHOR = "BBBBB";

    @Inject
    private CkbzRepository ckbzRepository;

    @Inject
    private CkbzSearchRepository ckbzSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCkbzMockMvc;

    private Ckbz ckbz;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CkbzResource ckbzResource = new CkbzResource();
        ReflectionTestUtils.setField(ckbzResource, "ckbzSearchRepository", ckbzSearchRepository);
        ReflectionTestUtils.setField(ckbzResource, "ckbzRepository", ckbzRepository);
        this.restCkbzMockMvc = MockMvcBuilders.standaloneSetup(ckbzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        ckbzSearchRepository.deleteAll();
        ckbz = new Ckbz();
        ckbz.setQuestion(DEFAULT_QUESTION);
        ckbz.setAnswer(DEFAULT_ANSWER);
        ckbz.setAuthor(DEFAULT_AUTHOR);
    }

    @Test
    @Transactional
    public void createCkbz() throws Exception {
        int databaseSizeBeforeCreate = ckbzRepository.findAll().size();

        // Create the Ckbz

        restCkbzMockMvc.perform(post("/api/ckbzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(ckbz)))
                .andExpect(status().isCreated());

        // Validate the Ckbz in the database
        List<Ckbz> ckbzs = ckbzRepository.findAll();
        assertThat(ckbzs).hasSize(databaseSizeBeforeCreate + 1);
        Ckbz testCkbz = ckbzs.get(ckbzs.size() - 1);
        assertThat(testCkbz.getQuestion()).isEqualTo(DEFAULT_QUESTION);
        assertThat(testCkbz.getAnswer()).isEqualTo(DEFAULT_ANSWER);
        assertThat(testCkbz.getAuthor()).isEqualTo(DEFAULT_AUTHOR);

        // Validate the Ckbz in ElasticSearch
        Ckbz ckbzEs = ckbzSearchRepository.findOne(testCkbz.getId());
        assertThat(ckbzEs).isEqualToComparingFieldByField(testCkbz);
    }

    @Test
    @Transactional
    public void getAllCkbzs() throws Exception {
        // Initialize the database
        ckbzRepository.saveAndFlush(ckbz);

        // Get all the ckbzs
        restCkbzMockMvc.perform(get("/api/ckbzs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(ckbz.getId().intValue())))
                .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION.toString())))
                .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER.toString())))
                .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())));
    }

    @Test
    @Transactional
    public void getCkbz() throws Exception {
        // Initialize the database
        ckbzRepository.saveAndFlush(ckbz);

        // Get the ckbz
        restCkbzMockMvc.perform(get("/api/ckbzs/{id}", ckbz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(ckbz.getId().intValue()))
            .andExpect(jsonPath("$.question").value(DEFAULT_QUESTION.toString()))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER.toString()))
            .andExpect(jsonPath("$.author").value(DEFAULT_AUTHOR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCkbz() throws Exception {
        // Get the ckbz
        restCkbzMockMvc.perform(get("/api/ckbzs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCkbz() throws Exception {
        // Initialize the database
        ckbzRepository.saveAndFlush(ckbz);
        ckbzSearchRepository.save(ckbz);
        int databaseSizeBeforeUpdate = ckbzRepository.findAll().size();

        // Update the ckbz
        Ckbz updatedCkbz = new Ckbz();
        updatedCkbz.setId(ckbz.getId());
        updatedCkbz.setQuestion(UPDATED_QUESTION);
        updatedCkbz.setAnswer(UPDATED_ANSWER);
        updatedCkbz.setAuthor(UPDATED_AUTHOR);

        restCkbzMockMvc.perform(put("/api/ckbzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCkbz)))
                .andExpect(status().isOk());

        // Validate the Ckbz in the database
        List<Ckbz> ckbzs = ckbzRepository.findAll();
        assertThat(ckbzs).hasSize(databaseSizeBeforeUpdate);
        Ckbz testCkbz = ckbzs.get(ckbzs.size() - 1);
        assertThat(testCkbz.getQuestion()).isEqualTo(UPDATED_QUESTION);
        assertThat(testCkbz.getAnswer()).isEqualTo(UPDATED_ANSWER);
        assertThat(testCkbz.getAuthor()).isEqualTo(UPDATED_AUTHOR);

        // Validate the Ckbz in ElasticSearch
        Ckbz ckbzEs = ckbzSearchRepository.findOne(testCkbz.getId());
        assertThat(ckbzEs).isEqualToComparingFieldByField(testCkbz);
    }

    @Test
    @Transactional
    public void deleteCkbz() throws Exception {
        // Initialize the database
        ckbzRepository.saveAndFlush(ckbz);
        ckbzSearchRepository.save(ckbz);
        int databaseSizeBeforeDelete = ckbzRepository.findAll().size();

        // Get the ckbz
        restCkbzMockMvc.perform(delete("/api/ckbzs/{id}", ckbz.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean ckbzExistsInEs = ckbzSearchRepository.exists(ckbz.getId());
        assertThat(ckbzExistsInEs).isFalse();

        // Validate the database is empty
        List<Ckbz> ckbzs = ckbzRepository.findAll();
        assertThat(ckbzs).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCkbz() throws Exception {
        // Initialize the database
        ckbzRepository.saveAndFlush(ckbz);
        ckbzSearchRepository.save(ckbz);

        // Search the ckbz
        restCkbzMockMvc.perform(get("/api/_search/ckbzs?query=id:" + ckbz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ckbz.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION.toString())))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER.toString())))
            .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())));
    }
}
