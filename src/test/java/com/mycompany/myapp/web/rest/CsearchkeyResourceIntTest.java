package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.L4App;
import com.mycompany.myapp.domain.Csearchkey;
import com.mycompany.myapp.repository.CsearchkeyRepository;
import com.mycompany.myapp.repository.search.CsearchkeySearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CsearchkeyResource REST controller.
 *
 * @see CsearchkeyResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = L4App.class)
@WebAppConfiguration
@IntegrationTest
public class CsearchkeyResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    private static final Integer DEFAULT_NB = 1;
    private static final Integer UPDATED_NB = 2;

    @Inject
    private CsearchkeyRepository csearchkeyRepository;

    @Inject
    private CsearchkeySearchRepository csearchkeySearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCsearchkeyMockMvc;

    private Csearchkey csearchkey;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CsearchkeyResource csearchkeyResource = new CsearchkeyResource();
        ReflectionTestUtils.setField(csearchkeyResource, "csearchkeySearchRepository", csearchkeySearchRepository);
        ReflectionTestUtils.setField(csearchkeyResource, "csearchkeyRepository", csearchkeyRepository);
        this.restCsearchkeyMockMvc = MockMvcBuilders.standaloneSetup(csearchkeyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        csearchkeySearchRepository.deleteAll();
        csearchkey = new Csearchkey();
        csearchkey.setName(DEFAULT_NAME);
        csearchkey.setNb(DEFAULT_NB);
    }

    @Test
    @Transactional
    public void createCsearchkey() throws Exception {
        int databaseSizeBeforeCreate = csearchkeyRepository.findAll().size();

        // Create the Csearchkey

        restCsearchkeyMockMvc.perform(post("/api/csearchkeys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(csearchkey)))
                .andExpect(status().isCreated());

        // Validate the Csearchkey in the database
        List<Csearchkey> csearchkeys = csearchkeyRepository.findAll();
        assertThat(csearchkeys).hasSize(databaseSizeBeforeCreate + 1);
        Csearchkey testCsearchkey = csearchkeys.get(csearchkeys.size() - 1);
        assertThat(testCsearchkey.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCsearchkey.getNb()).isEqualTo(DEFAULT_NB);

        // Validate the Csearchkey in ElasticSearch
        Csearchkey csearchkeyEs = csearchkeySearchRepository.findOne(testCsearchkey.getId());
        assertThat(csearchkeyEs).isEqualToComparingFieldByField(testCsearchkey);
    }

    @Test
    @Transactional
    public void getAllCsearchkeys() throws Exception {
        // Initialize the database
        csearchkeyRepository.saveAndFlush(csearchkey);

        // Get all the csearchkeys
        restCsearchkeyMockMvc.perform(get("/api/csearchkeys?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(csearchkey.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].nb").value(hasItem(DEFAULT_NB)));
    }

    @Test
    @Transactional
    public void getCsearchkey() throws Exception {
        // Initialize the database
        csearchkeyRepository.saveAndFlush(csearchkey);

        // Get the csearchkey
        restCsearchkeyMockMvc.perform(get("/api/csearchkeys/{id}", csearchkey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(csearchkey.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.nb").value(DEFAULT_NB));
    }

    @Test
    @Transactional
    public void getNonExistingCsearchkey() throws Exception {
        // Get the csearchkey
        restCsearchkeyMockMvc.perform(get("/api/csearchkeys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCsearchkey() throws Exception {
        // Initialize the database
        csearchkeyRepository.saveAndFlush(csearchkey);
        csearchkeySearchRepository.save(csearchkey);
        int databaseSizeBeforeUpdate = csearchkeyRepository.findAll().size();

        // Update the csearchkey
        Csearchkey updatedCsearchkey = new Csearchkey();
        updatedCsearchkey.setId(csearchkey.getId());
        updatedCsearchkey.setName(UPDATED_NAME);
        updatedCsearchkey.setNb(UPDATED_NB);

        restCsearchkeyMockMvc.perform(put("/api/csearchkeys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCsearchkey)))
                .andExpect(status().isOk());

        // Validate the Csearchkey in the database
        List<Csearchkey> csearchkeys = csearchkeyRepository.findAll();
        assertThat(csearchkeys).hasSize(databaseSizeBeforeUpdate);
        Csearchkey testCsearchkey = csearchkeys.get(csearchkeys.size() - 1);
        assertThat(testCsearchkey.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCsearchkey.getNb()).isEqualTo(UPDATED_NB);

        // Validate the Csearchkey in ElasticSearch
        Csearchkey csearchkeyEs = csearchkeySearchRepository.findOne(testCsearchkey.getId());
        assertThat(csearchkeyEs).isEqualToComparingFieldByField(testCsearchkey);
    }

    @Test
    @Transactional
    public void deleteCsearchkey() throws Exception {
        // Initialize the database
        csearchkeyRepository.saveAndFlush(csearchkey);
        csearchkeySearchRepository.save(csearchkey);
        int databaseSizeBeforeDelete = csearchkeyRepository.findAll().size();

        // Get the csearchkey
        restCsearchkeyMockMvc.perform(delete("/api/csearchkeys/{id}", csearchkey.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean csearchkeyExistsInEs = csearchkeySearchRepository.exists(csearchkey.getId());
        assertThat(csearchkeyExistsInEs).isFalse();

        // Validate the database is empty
        List<Csearchkey> csearchkeys = csearchkeyRepository.findAll();
        assertThat(csearchkeys).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCsearchkey() throws Exception {
        // Initialize the database
        csearchkeyRepository.saveAndFlush(csearchkey);
        csearchkeySearchRepository.save(csearchkey);

        // Search the csearchkey
        restCsearchkeyMockMvc.perform(get("/api/_search/csearchkeys?query=id:" + csearchkey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(csearchkey.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].nb").value(hasItem(DEFAULT_NB)));
    }
}
