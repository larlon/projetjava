package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.L4App;
import com.mycompany.myapp.domain.Cchatmessz;
import com.mycompany.myapp.repository.CchatmesszRepository;
import com.mycompany.myapp.repository.search.CchatmesszSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CchatmesszResource REST controller.
 *
 * @see CchatmesszResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = L4App.class)
@WebAppConfiguration
@IntegrationTest
public class CchatmesszResourceIntTest {


    private static final Integer DEFAULT_IDCHAT = 1;
    private static final Integer UPDATED_IDCHAT = 2;
    private static final String DEFAULT_AUTHOR = "AAAAA";
    private static final String UPDATED_AUTHOR = "BBBBB";
    private static final String DEFAULT_CONTENT = "AAAAA";
    private static final String UPDATED_CONTENT = "BBBBB";

    @Inject
    private CchatmesszRepository cchatmesszRepository;

    @Inject
    private CchatmesszSearchRepository cchatmesszSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCchatmesszMockMvc;

    private Cchatmessz cchatmessz;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CchatmesszResource cchatmesszResource = new CchatmesszResource();
        ReflectionTestUtils.setField(cchatmesszResource, "cchatmesszSearchRepository", cchatmesszSearchRepository);
        ReflectionTestUtils.setField(cchatmesszResource, "cchatmesszRepository", cchatmesszRepository);
        this.restCchatmesszMockMvc = MockMvcBuilders.standaloneSetup(cchatmesszResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        cchatmesszSearchRepository.deleteAll();
        cchatmessz = new Cchatmessz();
        cchatmessz.setIdchat(DEFAULT_IDCHAT);
        cchatmessz.setAuthor(DEFAULT_AUTHOR);
        cchatmessz.setContent(DEFAULT_CONTENT);
    }

    @Test
    @Transactional
    public void createCchatmessz() throws Exception {
        int databaseSizeBeforeCreate = cchatmesszRepository.findAll().size();

        // Create the Cchatmessz

        restCchatmesszMockMvc.perform(post("/api/cchatmesszs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cchatmessz)))
                .andExpect(status().isCreated());

        // Validate the Cchatmessz in the database
        List<Cchatmessz> cchatmesszs = cchatmesszRepository.findAll();
        assertThat(cchatmesszs).hasSize(databaseSizeBeforeCreate + 1);
        Cchatmessz testCchatmessz = cchatmesszs.get(cchatmesszs.size() - 1);
        assertThat(testCchatmessz.getIdchat()).isEqualTo(DEFAULT_IDCHAT);
        assertThat(testCchatmessz.getAuthor()).isEqualTo(DEFAULT_AUTHOR);
        assertThat(testCchatmessz.getContent()).isEqualTo(DEFAULT_CONTENT);

        // Validate the Cchatmessz in ElasticSearch
        Cchatmessz cchatmesszEs = cchatmesszSearchRepository.findOne(testCchatmessz.getId());
        assertThat(cchatmesszEs).isEqualToComparingFieldByField(testCchatmessz);
    }

    @Test
    @Transactional
    public void getAllCchatmesszs() throws Exception {
        // Initialize the database
        cchatmesszRepository.saveAndFlush(cchatmessz);

        // Get all the cchatmesszs
        restCchatmesszMockMvc.perform(get("/api/cchatmesszs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cchatmessz.getId().intValue())))
                .andExpect(jsonPath("$.[*].idchat").value(hasItem(DEFAULT_IDCHAT)))
                .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
                .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())));
    }

    @Test
    @Transactional
    public void getCchatmessz() throws Exception {
        // Initialize the database
        cchatmesszRepository.saveAndFlush(cchatmessz);

        // Get the cchatmessz
        restCchatmesszMockMvc.perform(get("/api/cchatmesszs/{id}", cchatmessz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cchatmessz.getId().intValue()))
            .andExpect(jsonPath("$.idchat").value(DEFAULT_IDCHAT))
            .andExpect(jsonPath("$.author").value(DEFAULT_AUTHOR.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCchatmessz() throws Exception {
        // Get the cchatmessz
        restCchatmesszMockMvc.perform(get("/api/cchatmesszs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCchatmessz() throws Exception {
        // Initialize the database
        cchatmesszRepository.saveAndFlush(cchatmessz);
        cchatmesszSearchRepository.save(cchatmessz);
        int databaseSizeBeforeUpdate = cchatmesszRepository.findAll().size();

        // Update the cchatmessz
        Cchatmessz updatedCchatmessz = new Cchatmessz();
        updatedCchatmessz.setId(cchatmessz.getId());
        updatedCchatmessz.setIdchat(UPDATED_IDCHAT);
        updatedCchatmessz.setAuthor(UPDATED_AUTHOR);
        updatedCchatmessz.setContent(UPDATED_CONTENT);

        restCchatmesszMockMvc.perform(put("/api/cchatmesszs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCchatmessz)))
                .andExpect(status().isOk());

        // Validate the Cchatmessz in the database
        List<Cchatmessz> cchatmesszs = cchatmesszRepository.findAll();
        assertThat(cchatmesszs).hasSize(databaseSizeBeforeUpdate);
        Cchatmessz testCchatmessz = cchatmesszs.get(cchatmesszs.size() - 1);
        assertThat(testCchatmessz.getIdchat()).isEqualTo(UPDATED_IDCHAT);
        assertThat(testCchatmessz.getAuthor()).isEqualTo(UPDATED_AUTHOR);
        assertThat(testCchatmessz.getContent()).isEqualTo(UPDATED_CONTENT);

        // Validate the Cchatmessz in ElasticSearch
        Cchatmessz cchatmesszEs = cchatmesszSearchRepository.findOne(testCchatmessz.getId());
        assertThat(cchatmesszEs).isEqualToComparingFieldByField(testCchatmessz);
    }

    @Test
    @Transactional
    public void deleteCchatmessz() throws Exception {
        // Initialize the database
        cchatmesszRepository.saveAndFlush(cchatmessz);
        cchatmesszSearchRepository.save(cchatmessz);
        int databaseSizeBeforeDelete = cchatmesszRepository.findAll().size();

        // Get the cchatmessz
        restCchatmesszMockMvc.perform(delete("/api/cchatmesszs/{id}", cchatmessz.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean cchatmesszExistsInEs = cchatmesszSearchRepository.exists(cchatmessz.getId());
        assertThat(cchatmesszExistsInEs).isFalse();

        // Validate the database is empty
        List<Cchatmessz> cchatmesszs = cchatmesszRepository.findAll();
        assertThat(cchatmesszs).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCchatmessz() throws Exception {
        // Initialize the database
        cchatmesszRepository.saveAndFlush(cchatmessz);
        cchatmesszSearchRepository.save(cchatmessz);

        // Search the cchatmessz
        restCchatmesszMockMvc.perform(get("/api/_search/cchatmesszs?query=id:" + cchatmessz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cchatmessz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idchat").value(hasItem(DEFAULT_IDCHAT)))
            .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())));
    }
}
